'use strict';
	var GulpConfig = (function () {
		function gulpConfig() {

				// Prequisites
				this.source					=		'./src/';
				this.build					=		'./build/';
				this.sourceApp		 		=		this.source + 'app/';

				// Dev Build
				//this.listFilesTS			=	this.source + '**/*.ts';
				this.listFilesSCSS			=	this.source + 'css/**/*+(scss|sass|css)';
				this.listFilesHTML			=	this.source + '**/*.html';
				this.listFilesJS				=	this.sourceApp + '**/*.js';
				this.listFilesFonts 		=	this.source + 'fonts/*+(eot)';
				this.listFilesCompileHTML 	= 	this.sourceApp+'**/*.html';
				this.listFilesImages		=	this.source + 'img/**/*+(png|jpg|gif)';
				this.injectConfig	  		=	{relative: false, addRootSlash: true, ignorePath: './build'};

				// Typings
				// this.typings				= 	'./typings/';
				// this.libraryTypeScriptDefinitions = './typings/**/*.ts';

				// Production Build
				this.jsOutputPath		  	=	this.build	+ 'js/';
				this.BuildPath					=	this.build + 'app/';
				this.cssOutputPath			=	this.build + 'css/';
				this.imgOutputPath			=	this.build + 'img/';
				this.fontsOutputPath		=	this.build + 'fonts/';
				this.htmlOutputPath			=	this.build + 'html/';

				var appconstants = {
					default: {
						apiHost: ''
					},
					development: {
						apiHost: 'http://localhost:3001'
					},
					staging: {
						apiHost: 'http://staging.example.com/api/'
					},
					production: {
						apiHost: 'http://olnotes.com/api/'
					}
				};

				this.appconstants = function(){
					return appconstants;
				};

				var run = {
					default: {
						js: {
							uglify: false
						},
						css: {

							cssnano: false
						}
					},
					development: {
						js: {
							uglify: false
						},
						css: {
							cssnano: false
						}
					},
					staging: {
						js: {
							uglify: true
						},
						css: {
							cssnano: true
						}
					},
					production: {
						js: {
							uglify: true
						},
						css: {
							cssnano: true
						}
					}
				};

				this.runconstant = function(){
					return run;
				}
			}
			return gulpConfig;
	})();
module.exports = GulpConfig;
