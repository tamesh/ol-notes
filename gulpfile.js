var gulp              	= require('gulp');
//var gulptypescript    = require('gulp-typescript');
//var tscCnfg           = require('./tsconfig.json');
var sass              	= require('gulp-sass'); //Sass Compiler
var cache             	= require('gulp-cache'); // Maintain Cache
var browserSync       	= require('browser-sync').create(); //Reload browser wheneve changes made in file.
var superstatic       	= require('superstatic');
var wiredep           	= require('wiredep').stream; // bower component injector.
var inject            	= require('gulp-inject');
//var s3				= require('gulp-s3');
var GulpConfig      	= require('./gulpconfig');
var chalk 				= require('chalk');
var del 				= require('del');
// Create config
var config      		= new GulpConfig();
var runSequence 		= require('run-sequence');

gulp.task('clean', function (done) { del([config.build], done);});

/* SCSS/CSS compiler/Injector */
gulp.task('sass', function(){
	return gulp.src(config.listFilesSCSS)
		.pipe(sass())
		.pipe(gulp.dest(config.cssOutputPath))
		.pipe( browserSync.reload({ stream: true}) );
});

/* Font injector */
gulp.task('fonts', function() {
	return gulp.src(config.listFilesFonts)
		.pipe(gulp.dest(config.fontsOutputPath));
});

/* JS injector */
gulp.task('js', function(){
	return gulp.src(config.listFilesJS)
		.pipe(gulp.dest(config.jsOutputPath));
});

/* Images injector */
gulp.task('img', function(){
	return gulp.src(config.listFilesImages)
		.pipe(gulp.dest(config.imgOutputPath));
});

/* HTML file Injector */
gulp.task('html', function() {
		//config.listFilesHTML
		return gulp.src([config.listFilesHTML])
				.pipe(gulp.dest(config.build))
				.pipe( browserSync.reload({ stream: true}) );
});

/* HTML file compiler */
gulp.task('compilehtml', function() {
		//config.listFilesHTML
		return gulp.src([config.listFilesCompileHTML])
				.pipe(gulp.dest(config.build))
				.pipe( browserSync.reload({ stream: true}) );
});

gulp.task('views', ['img','sass', 'fonts', 'js', 'html']);

gulp.task('inject', ['views'], function(){
	var sources = gulp.src([config.cssOutputPath+'**/*.css', config.jsOutputPath+'**/*.js'], {read: false});
	return gulp.src(config.source+'index.html')
		.pipe(inject(sources, config.injectConfig))
		.pipe(gulp.dest(config.build))
		.pipe(wiredep({ devDependencies: true }))
		.pipe(gulp.dest(config.build));
});

gulp.task('watch', function(){
		console.log(chalk.green('\n\nStart Watching \n\n'));
		//gulp.watch('src/**/*.ts', ['ts2js']);
		gulp.watch(config.listFilesHTML, ['compilehtml']);
		gulp.watch(config.listFilesSCSS, ['sass']);
		gulp.watch(config.listFilesJS, ['js']);
		gulp.watch(config.listFilesFonts, ['fonts']);
		gulp.watch(config.listFilesImages, ['img']);
		browserSync.reload({stream: true});
});

gulp.task('BS', ['inject'], function () {
	process.stdout.write(chalk.green('\n\nStarting browserSync and superstatic... \n\n'));
	browserSync.init({
		server: {
			baseDir       : 'build/',
			online        : false,
			middleware    : superstatic({
				debug       : true
			})
		},
		port            : 4000,
		injectChanges   : true,
		notify          : true,
		reloadDelay     : 0
	})
});

// gulp.task('deploy', ['ts2js', 'inject'], function() {
// 	gulp.src(config.build + '/**')
// 		.pipe(s3(config.aws, config.awsOptions));
// });

gulp.task('default', ['BS', 'watch']);
