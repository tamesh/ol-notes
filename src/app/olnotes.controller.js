(function() {
	'use strict';
	angular
		.module('olnotes.controller', [])
		.controller('MainController', ['$scope', MainController])
		.controller('HomeController',['$scope','localStorageService','$state', HomeController]);


	function MainController($scope) {
	}

	function HomeController($scope, localStorageService, $state) {
		$scope.submit = submit;
		$scope.user = {
			email: "",
			password: ""
		};

		if(localStorageService.get('logged-in')){
			$state.go('app.dashboard');
		}else{
			$state.go('home');
		}

		function submit(){
			if($scope.user.email === "admin" && $scope.user.password === "admin"){
				$state.go('app.dashboard');
				$('.modal-backdrop').remove();
				return localStorageService.set("logged-in", true);

			}
		}
	}


})();
