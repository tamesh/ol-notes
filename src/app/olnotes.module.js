(function() {
	'use strict';
	angular
		.module('olnotes', [
			'ui.router',
			'olnotes.header',
			'olnotes.footer',
			'olnotes.controller',
			'olnotes.dashboard',
			'olnotes.profile',
			'LocalStorageModule'
		])
		.config(['$stateProvider', '$urlRouterProvider','$locationProvider', routeconfig]);

		function routeconfig($stateProvider, $urlRouterProvider, $locationProvider){
			$urlRouterProvider.otherwise('/');
			$stateProvider
				.state('home',{
					url:'/',
					templateUrl: 'app/home.html',
					controller: 'HomeController'
				})
				.state('app',{
					url:'/app',
					views: {
						'': {
								resolve: {},
								templateUrl: 'app/maintemplate.html'
							},
						'header@app': {
							templateUrl: 'app/header/header.html',
							controller: 'HeaderController'
						},
						'content@app': {
							controller: function($state) {
								console.log('route start', $state);
							}
						},
						'footer@app': {
							templateUrl: 'app/footer/footer.html',
							controller: 'FooterController'
						}
					},

				})
				.state('app.dashboard', {
					url: '/dashboard',
					views: {
							'content@app': {
								templateUrl: 'app/dashboard/dashboard.html',
								controller: 'DashboardController'
							}
					},
				})
				.state('app.profile', {
					url: '/profile',
					views:{
						'content@app':{
							templateUrl: 'app/profile/profile.html',
							controller: 'ProfileController'
						}
					}
				});

			$locationProvider.html5Mode({
				 enabled: false,
				 requireBase: false
		  });
		}
})();
